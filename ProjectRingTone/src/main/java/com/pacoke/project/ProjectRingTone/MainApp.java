package com.pacoke.project.ProjectRingTone;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.logging.*;

import commons.Settings;

public class MainApp 
{
    public static void main( String[] args )
    {
    	Log log = LogFactory.getLog(MainApp.class);
    	//long start = System.nanoTime();
    	ServerSocket server = null;
    	try {
    		log.info("La aplicacion a arrancado.");
    		server = new ServerSocket(503);
			Socket socket = server.accept();
			
			///Starts states here and share variable to notify when detect high frecuency. 
			
			if(Settings.configurada){
				log.info("La aplicacion ya esta configurada.");
				InetAddress IP=InetAddress.getLocalHost();
				System.out.println(IP.getHostAddress());
			}else{
				
			}
    	} catch (IOException e1) {
			log.error("Se ha cerrado el Socket - "+ e1.getMessage(),e1);
		}finally{
			try {
				server.close();
			} catch (IOException e) {
				log.error("Se ha cerrado el Socket - "+ e.getMessage(),e);
			}
		}
    	/*
    	StartAplication app = new StartAplication(2);
    	app.processAudio(5);
    	long end = System.nanoTime();
    	System.out.println("Tiempo: "+ (end-start));*/
    	
    }
    
    public static int getIndex(int freq,int[] RANGE) {
    	    int i = 0;
    	    while(RANGE[i] < freq) i++;
    	        return i;
    	    }
    	}
