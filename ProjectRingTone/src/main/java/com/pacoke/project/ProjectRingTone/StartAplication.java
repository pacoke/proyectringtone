package com.pacoke.project.ProjectRingTone;

import java.io.ByteArrayOutputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import listenerMicrophone.Microphone;

public class StartAplication {
	
	private AudioFormat format;
	private DataLine.Info info;
	private byte[] buffer;
	private int sizeBuffer;
	private TargetDataLine line;
	private int Harvester = 4096;
	
	public StartAplication(int sizeBuffer){
		
		try {
			this.sizeBuffer = sizeBuffer;
			this.format = getFormat();
			this.buffer = new byte[sizeBuffer];
			this.info = new DataLine.Info(TargetDataLine.class, this.format);
			this.line = (TargetDataLine) AudioSystem.getLine(this.info);
		} catch (LineUnavailableException e) {
			
			System.out.println(e.getMessage());
		}
	}
	
	public void processAudio(int segundos){
		
		try {
			Microphone mic = new Microphone(this.line,this.buffer);
			Thread hilo = new Thread(mic);
			hilo.start();
			this.line.open(this.format);
			this.line.start();
			Thread.sleep(segundos*1000);
			mic.setRunning(false);
			Complex[][] fe = changeDomainTimeToFrecuency(mic.getOut());
		} catch (LineUnavailableException | InterruptedException e) {
			System.out.println(e.getMessage());
		}
	};
	private Complex[][] changeDomainTimeToFrecuency(ByteArrayOutputStream datas){
		byte audio[]= datas.toByteArray();
		int totalSize = audio.length;
		int amountPossible = totalSize/Harvester;
		FastFourierTransformer FFT = new FastFourierTransformer(DftNormalization.STANDARD);
		Complex[][] results = new Complex[amountPossible][];
		//For all the chunks:
		for(int times = 0;times < amountPossible; times++) {
		    Complex[] complex = new Complex[Harvester];
		    for(int i = 0;i < Harvester;i++) {
		        //Put the time domain data into a complex number with imaginary part as 0:
		        complex[i] = new Complex(audio[(times*Harvester)+i], 0);
		    }
		    //Perform FFT analysis on the chunk:
		    results[times] = FFT.transform(complex, TransformType.FORWARD);
		}
		return results;
	}
	private AudioFormat getFormat() {
	    float sampleRate = 44100;
	    int sampleSizeInBits = 8 * this.sizeBuffer;
	    int channels = 1; //mono
	    boolean signed = true;
	    boolean bigEndian = true;
	    return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
	}

}
