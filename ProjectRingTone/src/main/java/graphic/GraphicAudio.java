package graphic;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;

import org.apache.commons.math3.complex.Complex;

public class GraphicAudio extends Canvas{
	
	private Complex[][] results;
	private int totalSize;
	
	public GraphicAudio(Complex[][] results,int totalSize){
        this.results = results;
        this.totalSize = totalSize;
	}
	public void paint(Graphics g){
		Boolean logModeEnabled = true;
		for(int i = 0; i < results.length; i++) {
		    int freq = 1;
		    for(int line = 1; line < totalSize; line++) {
		        // To get the magnitude of the sound at a given frequency slice
		        // get the abs() from the complex number.
		        // In this case I use Math.log to get a more managable number (used for color)
		        double magnitude = Math.log(results[i][freq].abs()+1);
		
		        // The more blue in the color the more intensity for a given frequency point:
		        g.setColor(new Color(0,(int)magnitude*10,(int)magnitude*20));
		        // Fill:
		        g.fillRect(i*2, (totalSize-line)*3,500,500);
		        // I used a improviced logarithmic scale and normal scale:
		        if (logModeEnabled && (Math.log10(line) * Math.log10(line)) > 1) {
		            freq += (int) (Math.log10(line) * Math.log10(line));
		        } else {
		            freq++;
		        }
		    }
}


	}

}
