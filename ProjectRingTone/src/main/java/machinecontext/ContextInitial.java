package machinecontext;
import machineStates.*;
public class ContextInitial implements IContext {
	
	private State Currentstate;
	private State NextState;
 	
	public ContextInitial(){
		
	}
	
 	public void request()
 	{
 		
 	}

	public State getCurrentstate() {
		return Currentstate;
	}

	public void setCurrentstate(State currentstate) {
		Currentstate = currentstate;
	}

	public State getNextState() {
		return NextState;
	}

	public void setNextState(State nextState) {
		NextState = nextState;
	}

}
