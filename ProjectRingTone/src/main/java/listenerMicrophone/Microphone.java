package listenerMicrophone;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.sound.sampled.TargetDataLine;

public class Microphone implements Runnable{
	private Boolean running;
	private ByteArrayOutputStream out;
	private TargetDataLine line;
	private byte[] buffer;
	
	
	public Microphone(Boolean running,ByteArrayOutputStream out)
	{
		this.running = running;
		this.out = out;
	}
	public Microphone(TargetDataLine line,byte[] buffer){
		this.line = line;
		this.buffer = buffer;
	}
	
	@Override
	public void run() {
		this.out = new ByteArrayOutputStream();
		this.running = true;
		try {
		    while (running) {
				int count = this.line.read(this.buffer, 0, this.buffer.length);
		        if (count > 0) {
		            out.write(buffer, 0, count);
		            System.out.println(stringByte(buffer));
		        }
		    }
		    out.close();
		} catch (IOException e) {
		    System.err.println("I/O problems: " + e);
		    System.exit(-1);
		}
	}
	private String stringByte(byte[] s){
		String res="";
		for(int i=0;i<s.length;i++)
		{
			res=res+"-"+s[i];
		}
		return res;
	}

	public Boolean getRunning() {
		return running;
	}

	public void setRunning(Boolean running) {
		this.running = running;
	}

	public ByteArrayOutputStream getOut() {
		return out;
	}

	public void setOut(ByteArrayOutputStream out) {
		this.out = out;
	}
	
	public TargetDataLine getLine() {
		return line;
	}
	
	public void setLine(TargetDataLine line) {
		this.line = line;
	}
	
	public byte[] getBuffer() {
		return buffer;
	}
	
	public void setBuffer(byte[] buffer) {
		this.buffer = buffer;
	}
	

}
